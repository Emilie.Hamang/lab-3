package no.uib.inf101.terminal;

public interface Command {
    // legge til metodesignaturer i Command
    String run(String[] args);

    String getName();
}
