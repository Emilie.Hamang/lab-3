package no.uib.inf101.terminal;

public class CmdEcho implements Command {
    @Override
    public String getName() {
        return "echo";
    }

    @Override
    public String run(String[] args) {
        // gå igjennom args med er foreeach løkke
        // hver av argumentene er limt sammen med mellomrom
        // legg til et mellomrom etter hver av strengene
        // hivs det er {foo, bar } så skal retrverdien være "foo bar "
        String theString = "";
        for (String str : args) {
            theString += str + " ";
        }
        return theString;
    }
}
