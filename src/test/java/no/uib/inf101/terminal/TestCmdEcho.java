package no.uib.inf101.terminal;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class TestCmdEcho {
    @Test
    public void testGetName() {
        Command echo = new CmdEcho();
        assertEquals("echo", echo.getName());
    }

    @Test
    public void testRun() {
        Command echo = new CmdEcho();

        String[] args = new String[] { "foo", "bar" };
        assertEquals("foo bar ", echo.run(args));

        args = new String[] { "hello" };
        assertEquals("hello ", echo.run(args));

        args = new String[] {};
        assertEquals("", echo.run(args));
    }
}
